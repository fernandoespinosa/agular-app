'use strict';

describe('Controller: OtrocontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('tableAlanApp'));

  var OtrocontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OtrocontrollerCtrl = $controller('OtrocontrollerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OtrocontrollerCtrl.awesomeThings.length).toBe(3);
  });
});
