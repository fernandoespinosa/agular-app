'use strict';

describe('Controller: CreateEditPostCtrl', function () {

  // load the controller's module
  beforeEach(module('tableAlanApp'));

  var CreateEditPostCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CreateEditPostCtrl = $controller('CreateEditPostCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CreateEditPostCtrl.awesomeThings.length).toBe(3);
  });
});
