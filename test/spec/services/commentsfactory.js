'use strict';

describe('Service: commentsFactory', function () {

  // load the service's module
  beforeEach(module('tableAlanApp'));

  // instantiate service
  var commentsFactory;
  beforeEach(inject(function (_commentsFactory_) {
    commentsFactory = _commentsFactory_;
  }));

  it('should do something', function () {
    expect(!!commentsFactory).toBe(true);
  });

});
