'use strict';

describe('Filter: changeToBr', function () {

  // load the filter's module
  beforeEach(module('tableAlanApp'));

  // initialize a new instance of the filter before each test
  var changeToBr;
  beforeEach(inject(function ($filter) {
    changeToBr = $filter('changeToBr');
  }));

  it('should return the input prefixed with "changeToBr filter:"', function () {
    var text = 'angularjs';
    expect(changeToBr(text)).toBe('changeToBr filter: ' + text);
  });

});
