'use strict';

angular.module('tableAlanApp')
  .filter('changeToBr', function () {
      return function  (input) {
          if(input != null) {
              return input.replace(/\r?\n/g, '</br>');
          }
      }
  });
