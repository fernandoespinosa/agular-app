'use strict';

/**
 * @ngdoc service
 * @name tableAlanApp.dataService
 * @description
 * # dataService
 * Service in the tableAlanApp.
 */
angular.module('tableAlanApp')
  .service('dataService', function () {
      var miValor = 0;
      
      function getValor(){
          return miValor;
      }
      
      function setValor(valor) {
          miValor = valor
      }
    return{
        getValor : getValor,
        setValor : setValor
    }
  });
