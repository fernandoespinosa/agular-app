'use strict';

/**
 * @ngdoc service
 * @name tableAlanApp.usersFactory
 * @description
 * # usersFactory
 * Factory in the tableAlanApp.
 */
angular.module('tableAlanApp')
  .factory('usersFactory', function ($http) {
    // Service logic
    // ...

      function getUsers(){
          return $http.get('http://localhost:3000/users').then(function(users){
              return users.data;
          });
      }

      function getUser(idUser){
          return $http.get('http://localhost:3000/users/' + idUser).then(function(users){
              return users.data;
          });
      }

    // Public API here
    return {
        getUsers: getUsers,
        getUser : getUser
    };
  });
