'use strict';

/**
 * @ngdoc service
 * @name tableAlanApp.dataFactory
 * @description
 * # dataFactory
 * Factory in the tableAlanApp.
 */
angular.module('tableAlanApp')
  .factory('postsFactory', function ($http,usersFactory,commentsFactory) {
      var miValor = 0;

      function getValor(){
          return miValor;
      }

      function setValor(valor) {
          miValor = valor
      }

      function setImage(text){
          var images = [
              'abstract',
              'city',
              'people',
              'transport',
              'food',
              'nature',
              'sports',
              'technics',
              'cats',
              'fashion'
          ];
          var image = images[Math.floor((Math.random() * images.length))];
          //return 'http://lorempixel.com/770/514/'+image+'/';
          return 'https://placeholdit.imgix.net/~text?txtsize=25&txt='+text+'&w=320&h=240';
          //return 'https://assets.imgix.net/~text?txtfit=max&fit=crop&h=150&w=320&exp=-4&txt='+text+'&txtsize=25&txtfont64=QXZlbmlyLUJsYWNr&txtalign=center%2Cbottom';
      }

      function preparePosts(posts,users){
          angular.forEach(posts, function(post, key) {
              angular.forEach(users, function(user, key) {
                    if(post.userId == user.id){
                        post.user = user;
                        post.image = setImage(post.title);
                        post.date = getRandomDate();
                    }
              });
          });
          return posts;
      }

      function getPosts(){
          return $http.get('http://localhost:3000/posts').then(function(posts){
              var postsData =  posts.data;
              return usersFactory.getUsers().then(function (users) {
                    return preparePosts(postsData,users);
              })
          });
      }

      function getPost(idPost){
          return $http.get('http://localhost:3000/posts/' + idPost).then(function(post){
              var postData = post.data;
              return usersFactory.getUser(postData.userId).then(function(user){
                  postData.user = user;
                  postData.date = getRandomDate();
                  return commentsFactory.getPostComments(postData.id).then(function(comments){
                      postData.comments = comments;
                      return postData
                  });
              });
          })
      }

      function getRandomDate(){
          return new Date(new Date(2010, 0, 1).getTime() + Math.random() * (new Date().getTime() - new Date(2012, 0, 1).getTime()));
      }
      
      function addPost(newPost){
          newPost.userId = 1;
          return $http.post('http://localhost:3000/posts', newPost).then(function(post){
              return post.data;
          });
      }

      function editPost(Post){
          return $http.put('http://localhost:3000/posts/'+ Post.id, Post).then(function(post){
              return post.data;
          });
      }

      return{
          getValor : getValor,
          setValor : setValor,
          getPosts : getPosts,
          getPost : getPost,
          addPost : addPost,
          getRandomDate : getRandomDate,
          setImage  : setImage,
          editPost : editPost
      };
  });
