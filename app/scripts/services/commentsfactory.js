'use strict';

angular.module('tableAlanApp')
  .factory('commentsFactory', function ($http) {
    // Service logic

      function getPostComments(idUser){
          return $http.get('http://localhost:3000/posts/'+ idUser +'/comments').then(function(comments){
              return comments.data;
          });
      }

      function addComment(newComment){
          return $http.post('http://localhost:3000/posts/'+ newComment.idPost + '/comments',newComment).then(function(comments){
              return comments.data;
          });
      }
    // Public API here
    return {
        getPostComments: getPostComments,
        addComment : addComment
    };
  });
