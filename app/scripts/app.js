'use strict';

/**
 * @ngdoc overview
 * @name tableAlanApp
 * @description
 * # tableAlanApp
 *
 * Main module of the application.
 */
angular
    .module('tableAlanApp', [
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.bootstrap',
        'ui.router'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('main', {
                url: '/',
                templateUrl : 'views/main.html',
                controller: 'MainCtrl as main'
            })
            .state('posts', {
                abstract: true,
                template: '<ui-view/>'
            })
            .state('posts.list', {
                url: '/posts',
                templateUrl: 'views/posts.html',
                controller: 'PostsCtrl as postsCtrl'
            })
            .state('posts.detail', {
                url : '/posts/:idPost',
                templateUrl: 'views/post.html',
                controller: 'PostsCtrl as postsCtrl'
            });
    });
