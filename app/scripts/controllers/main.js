'use strict';

/**
 * @ngdoc function
 * @name tableAlanApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tableAlanApp
 */
angular.module('tableAlanApp')
  .controller('MainCtrl', function (dataService,postsFactory) {
      var vm = this;
      
      vm.dt = dataService;
      vm.df = postsFactory;
      
      vm.sumaAlgo = function(){
          dataService.setValor(dataService.getValor()+1);
      };

      vm.sumaAlgoFactory = function(){
          vm.df.setValor(vm.df.getValor()+1);
      };

      vm.suma = function(){
          vm.sumaAlgo();
          vm.sumaAlgoFactory();
      }
      
      
  });
