'use strict';

angular.module('tableAlanApp')
    .controller('PostsCtrl', function (dataService, postsFactory, $stateParams, commentsFactory, $uibModal, $log) {
        var vm = this;

        vm.posts = [];
        vm.post = [];
        vm.newComment = {};
        vm.maxSize = 8;
        vm.currentPage = 1;
        var idPost = $stateParams.idPost;

        vm.getPosts = function () {
            postsFactory.getPosts().then(function (posts) {
                vm.posts = posts;
                vm.totalItems = vm.posts.length;
                vm.numero = 0;
            });
        };

        vm.getPost = function () {
            postsFactory.getPost(idPost).then(function (post) {
                vm.post = post;
                vm.postBody = vm.post.body.replace(/\r?\n/g, '</br>');
                angular.forEach(vm.post.comments, function (comment, key) {
                    //vm.post.comments[key].commentBody = comment.body.replace(/\r?\n/g, '</br>');
                });
            });
        };

        vm.addComment = function (idPost) {
            vm.newComment.idPost = idPost;
            commentsFactory.addComment(vm.newComment).then(function (comment) {
                vm.post.comments.push(comment);
                //vm.post.comments[vm.post.comments.length - 1].commentBody = comment.body.replace(/\r?\n/g, '</br>');
            });
            vm.newComment = {};
        };

        vm.open = function (size) {

            var modalInstance = $uibModal.open({
                animation: vm.animationsEnabled,
                templateUrl: 'views/post-create-edit.html',
                controller: 'CreateEditPostCtrl as CreateEdit',
                size: size,
                resolve: {
                    post: function () {
                        return vm.post;
                    }
                }
            });

            modalInstance.result.then(function (post) {
                post.id = vm.posts.length + 1;
                vm.posts.push(post);
                vm.post = post;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        (idPost == null) ? vm.getPosts() : vm.getPost();
    });
