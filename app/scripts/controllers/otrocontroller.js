'use strict';

/**
 * @ngdoc function
 * @name tableAlanApp.controller:OtrocontrollerCtrl
 * @description
 * # OtrocontrollerCtrl
 * Controller of the tableAlanApp
 */
angular.module('tableAlanApp')
  .controller('OtrocontrollerCtrl', function (dataFactory) {
      
      var vm = this;
      vm.quieroValor = function () {
          return dataFactory.getValor();
      };
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
