'use strict';

/**
 * @ngdoc function
 * @name tableAlanApp.controller:CreateEditPostCtrl
 * @description
 * # CreateEditPostCtrl
 * Controller of the tableAlanApp
 */
angular.module('tableAlanApp')
  .controller('CreateEditPostCtrl', function ($uibModalInstance, post, postsFactory) {
      var vm = this;
      vm.post = {};
      var userName;
      var comments;
      var isNewPost = true;
      if(post.length != 0){
          isNewPost = false;
          vm.post.id = post.id;
          vm.post.title = post.title;
          vm.post.body = post.body;
          vm.post.userId = post.userId;
          comments = post.comments;
          userName = post.user.name;
      }
      vm.ok = function () {
          if (isNewPost) {
              postsFactory.addPost(vm.post).then(function (data) {
                  vm.post = data
              });
              vm.post.date = postsFactory.getRandomDate();
              vm.post.image = postsFactory.setImage(vm.post.title);
          }
          else{
              postsFactory.editPost(vm.post).then(function (data) {
                  vm.post = data;
              });
              vm.post.date = postsFactory.getRandomDate();
              vm.post.user = {
                  name: userName
              };
              vm.post.comments = comments;
          }
          $uibModalInstance.close(vm.post);
      };

      vm.cancel = function () {
          $uibModalInstance.dismiss('cancel');
      };
  });
